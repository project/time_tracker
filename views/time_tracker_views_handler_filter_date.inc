<?php

/**
 * @file
 * Time tracker's date views filter handler.
 */

/**
 * Filter to handle dates stored as a timestamp.
 */
class time_tracker_views_handler_filter_date extends views_handler_filter_date {

  /**
   * {@inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();

    // Value is already set up properly, we're just adding our new field to it.
    $options['value']['type']['default'] = 'date';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    if (!empty($form_state['values']['options']['expose']['optional'])) {
      // Who cares what the value is if it's exposed and optional.
      return;
    }

    $this->validate_valid_time($form['value'], $form_state['values']['options']['operator'], $form_state['values']['options']['value']);
  }

  /**
   * {@inheritdoc}
   */
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    if (empty($form_state['exposed'])) {
      $form['value']['type'] = array(
        '#type' => 'radios',
        '#title' => t('Value type'),
        '#options' => array(
          'date' => t('A date in any <a href="@url">php readable format</a>.', array(
            '@url' => 'http://php.net/manual/en/datetime.formats.php',
          )),
          'offset' => t('An offset from the current time such as "!example1" or "!example2"', array(
            '!example1' => '+1 day',
            '!example2' => '-2 hours -30 minutes',
          )),
          'date_popup' => t('Use a date popup field for date entry.'),
        ),
        '#default_value' => !empty($this->value['type']) ? $this->value['type'] : 'date',
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  function exposed_form(&$form, &$form_state) {
    parent::exposed_form($form, $form_state);
    // Get the filter identifier. This is defined in views in the filter
    // settings so it could vary from the usual 'timestamp', 'start', and 'end'.
    $filter_identifier = $this->options['expose']['identifier'];
    // Check if the date popup module is enabled.
    if ($this->options['value']['type'] == 'date_popup') {
      if (variable_get('time_entry_method', 'duration') == 'duration' || $filter_identifier == 'timestamp') {
        $format = variable_get('timestamp_date_format', 'F d, Y');
      }
      else {
        $format = variable_get('time_interval_date_format', 'h:i A - M d, Y');
      }

      if (isset($form[$filter_identifier]['#type'])) {
        $form[$filter_identifier]['#type'] = 'date_popup';
        $form[$filter_identifier]['#date_format'] = $format;
        // Process the value for relative timeframes.
        // I don't know why but the datepopup widget won't take our format for
        // the default value so we have to use one it understands.
        $form[$filter_identifier]['#default_value'] = date(
          'Y-M-d H:i:s',
          strtotime($form[$filter_identifier]['min']['#default_value'])
        );
      }
      elseif (isset($form['timestamp']['min']) && isset($form['timestamp']['max'])) {
        $form[$filter_identifier]['min']['#type'] = 'date_popup';
        $form[$filter_identifier]['min']['#date_format'] = $format;
        // Check if the default value is relative.
        $form[$filter_identifier]['min']['#default_value'] = date(
          'Y-m-d H:i:s',
          strtotime($form[$filter_identifier]['min']['#default_value'])
        );
        $form[$filter_identifier]['max']['#type'] = 'date_popup';
        $form[$filter_identifier]['max']['#date_format'] = $format;
        // Check if the default value is relative.
        $form[$filter_identifier]['max']['#default_value'] = date(
          'Y-m-d H:i:s',
          strtotime($form[$filter_identifier]['max']['#default_value'])
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  function exposed_validate(&$form, &$form_state) {
    if (empty($this->options['exposed'])) {
      return;
    }

    if (!empty($this->options['expose']['optional'])) {
      // Who cares what the value is if it's exposed and optional.
      return;
    }

    $value = &$form_state['values'][$this->options['expose']['identifier']];
    $value['type'] = $this->options['value']['type'];
    if (!empty($this->options['expose']['use_operator']) && !empty($this->options['expose']['operator'])) {
      $operator = $form_state['values'][$this->options['expose']['operator']];
    }
    else {
      $operator = $this->operator;
    }

    $this->validate_valid_time($form, $operator, $value);
  }

  /**
   * {@inheritdoc}
   */
  function accept_exposed_input($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }

    // Store this because it will get overwritten.
    $type = $this->value['type'];
    $rc = parent::accept_exposed_input($input);

    // Don't filter if value(s) are empty.
    $operators = $this->operators();
    if (!empty($this->options['expose']['use_operator']) && !empty($this->options['expose']['operator'])) {
      $operator = $input[$this->options['expose']['operator']];
    }
    else {
      $operator = $this->operator;
    }

    if ($operators[$operator]['values'] == 1) {
      if ($this->value['value'] == '') {
        return FALSE;
      }
    }
    else {
      if ($this->value['min'] == '' || $this->value['max'] == '') {
        return FALSE;
      }
    }

    // Restore what got overwritten by the parent.
    $this->value['type'] = $type;
    return $rc;
  }

  /**
   * {@inheritdoc}
   */
  function validate_valid_time(&$form, $operator, $value) {

    $operators = $this->operators();

    if ($operators[$operator]['values'] == 1 && $value['value']) {
      $convert = strtotime($value['value']);
      if ($convert == -1 || $convert === FALSE) {
        form_error($form['value'], t('Invalid date format.'));
      }
    }
    elseif ($operators[$operator]['values'] == 2) {
      // For validation, we only care that it parses.
      $min = strtotime($value['min']);
      $max = strtotime($value['max']);

      $filter_identifier = $this->options['expose']['identifier'];

      if ($min == -1 || $min === FALSE) {
        form_error($form[$filter_identifier]['min'], t('Invalid date format.'));
      }
      if ($max == -1 || $max === FALSE) {
        form_error($form[$filter_identifier]['max'], t('Invalid date format.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  function op_between($field) {
    $a = intval(strtotime($this->value['min']));
    $b = intval(strtotime($this->value['max']));

    // We want between to be inclusive, so we add 1 day - 1 second to the end
    // date.
    // TODO: make this a setting.
    $b += (86400 - 1);

    if ($this->value['type'] == 'offset') {
      // Let's make sure we keep the sign.
      $a = '***CURRENT_TIME***' . sprintf('%+d', $a);
      $b = '***CURRENT_TIME***' . sprintf('%+d', $b);
    }

    // This is safe because we are manually scrubbing the values.
    // It is necessary to do it this way because $a and $b are formulas when
    // using an offset.
    $operator = strtoupper($this->operator);
    $this->query->add_where_expression($this->options['group'], "$field $operator $a AND $b");
  }

  /**
   * {@inheritdoc}
   */
  function op_simple($field) {
    $value = intval(strtotime($this->value['value']));

    if ($this->value['type'] == 'offset') {
      // Let's make sure we keep the sign.
      $value = sprintf('***CURRENT_TIME***%+d', $value);
    }
    // This is safe because we are manually scrubbing the value.
    // It is necessary to do it this way because $value is a formula when using
    // an offset.
    $this->query->add_where_expression($this->options['group'], "$field $this->operator $value");
  }
}
