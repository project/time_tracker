<?php
/**
 * @file
 * Utility functions for activities.
 */

/**
 * We save the entity by calling the controller.
 *
 * @param object $entity
 *   The entity to save.
 *
 * @return object
 *   The entity we just saved.
 */
function time_tracker_activity_save(&$entity) {
  $controller = entity_get_controller('time_tracker_activity');
  if ($controller instanceof TimeTrackerActivityControllerInterface) {
    return $controller->save($entity);
  }

  return FALSE;
}

/**
 * Use the controller to delete the entity.
 *
 * @param object $entity
 *   The entity to delete.
 *
 * @return bool
 *   Whether the deletion was successful.
 */
function time_tracker_activity_delete($entity) {
  $controller = entity_get_controller('time_tracker_activity');
  if ($controller instanceof TimeTrackerActivityControllerInterface) {
    $controller->delete($entity);
    return TRUE;
  }

  return FALSE;
}

/**
 * Returns the activity name for a given activity id.
 *
 * @param mixed $activity_id
 *   The id of the activity.
 *
 * @return string
 *   The name of the activity
 */
function _time_tracker_activity_get_name($activity_id) {
  $name = db_query('SELECT tta.name FROM {time_tracker_activity} tta WHERE tta.taid = :taid', array(':taid' => $activity_id))->fetchField();

  return $name;
}

/**
 * Helper function to get the active activities.
 *
 * Formatted as a options list array for a select form element.
 *
 * @return array
 *   The active activities.
 */
function _time_tracker_activities_get_active() {
  $activities = array();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'time_tracker_activity')
    ->propertyCondition('status', 1);
  $result = $query->execute();

  if (isset($result['time_tracker_activity'])) {
    $activity_ids = array_keys($result['time_tracker_activity']);
    $active_activities = entity_load('time_tracker_activity', $activity_ids);

    foreach ($active_activities as $activity) {
      $activities[$activity->taid] = $activity->name;
    }
  }

  return $activities;
}
