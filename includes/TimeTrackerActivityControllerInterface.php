<?php

/**
 * @file
 * The TimeTrackerActivityController interface.
 */

/**
 * TimeTrackerActivityControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */
interface TimeTrackerActivityControllerInterface extends DrupalEntityControllerInterface {

  /**
   * Create a new time tracker entry entity.
   *
   * @return object
   *   The entity.
   */
  public function create();

  /**
   * Save a time tracker entry entity.
   *
   * @param object $entity
   *   The entity we want to save.
   *
   * @return object
   *   The entity we just saved.
   */
  public function save($entity);

  /**
   * Delete a time tracker entry entity.
   *
   * @param object $entity
   *   The entity we want to delete.
   */
  public function delete($entity);
}
