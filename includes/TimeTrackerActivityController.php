<?php
/**
 * @file
 * The TimeTrackerActivityController implementation.
 */

/**
 * TimeTrackerActivityController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class TimeTrackerActivityController extends DrupalDefaultEntityController implements TimeTrackerActivityControllerInterface {

  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);

    uasort($entities, function ($a, $b) {
      return $a->weight - $b->weight;
    });

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'time_tracker_activity';

    $entity->bundle = 'activity';
    $entity->item_description = '';
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity) {
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'time_tracker_activity');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // taid as the key.
    $primary_keys = isset($entity->taid) ? 'taid' : array();
    // Write out the entity record.
    drupal_write_record('time_tracker_activity', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('time_tracker_activity', $entity);
    }
    else {
      field_attach_update('time_tracker_activity', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'time_tracker_activity');

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function delete($entity) {
    $this->delete_multiple(array($entity));
  }

  /**
   * Delete one or more time_tracker_activity entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $entities
   *   An array of entities.
   *
   * @throws \Exception
   *   Throws an exception when the deletion operation throws one.
   */
  public function delete_multiple($entities) {
    $taids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          module_invoke_all('time_tracker_activity_delete', $entity);
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'time_tracker_activity');
          field_attach_delete('time_tracker_activity', $entity);
          $taids[] = $entity->taid;
        }
        db_delete('time_tracker_activity')
          ->condition('taid', $taids, '=')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('time_tracker', $e);
        throw $e;
      }
    }
  }
}
