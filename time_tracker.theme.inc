<?php
/**
 * @file
 * Theme functions for time_tracker.
 */

/**
 * Theme the activity table as a sortable list of activities.
 *
 * @param array $variables
 *   An array containing the form data.
 *
 * @return string
 *   The themed form.
 *
 * @see time_tracker_activity_table_form()
 */
function theme_time_tracker_activity_table($variables) {
  // For some reason form comes in an empty array...
  $form = $variables['form'];
  $output = '';

  if (!empty($form['activities'])) {
    // Add the table drag functionality to the page.
    drupal_add_tabledrag('activity-table', 'order', 'sibling', 'activity-weight');

    // The table headers.
    $header = array(
      t('Activity Name'),
      t('Weight'),
      t('Status'),
      array(
        'data' => t('Operations'),
        'colspan' => 1,
      ),
    );
    $rows = array();

    // Take all the activities and render into table array.
    foreach (element_children($form['activities']) as $key) {
      // Add class to group weight fields for drag and drop.
      $form['activities'][$key]['weight']['#attributes']['class'] = array('activity-weight');
      $rows[] = array(
        'data' => array(
          drupal_render($form['activities'][$key]['name']),
          drupal_render($form['activities'][$key]['weight']),
          drupal_render($form['activities'][$key]['status']),
          drupal_render($form['activities'][$key]['delete']),
        ),
        'class' => array('draggable'),
      );
    }

    $table = array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('id' => array('activity-table')),
    );
    $output .= theme('table', $table);
  }

  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Theme function for showing time entries on nodes.
 *
 * @param array $variables
 *   An array containing time_entries:
 *     An array of time entry objects containing at least:
 *       teid: The time entry id;
 *       nid: The node id this time entry is attached to;
 *       uid: The user id to whom this time entry belongs;
 *       cid: If applicable, the comment id for this time entry;
 *       activity: The time entry Activity;
 *       timestamp: The unix timestamp of when this time entry was dated;
 *       start: If tracking time by interval, the start time of the entry;
 *       end: If tracking time by interval, the end time of the entry;
 *       duration: The time entry duration in HOURS;
 *       note: Any notes about the time entry (empty for comments);
 *       billable: Whether this is billable or not (0 => no, 1 => yes);
 *       locked: Whether this entry has been locked, requires extra permissions.
 *
 * @return string
 *   The themed form.
 */
function theme_time_tracker_time_entry_table($variables) {
  global $user;
  $time_entries = $variables['time_entries'];

  // Use flags.
  $use_deductions = variable_get('enable_deductions_field', 0);
  $use_billable = variable_get('enable_billable_field', 0);
  $use_billed = variable_get('enable_billed_field', 0);
  // Does the user have ops permissions.
  $has_ops = user_access('edit any time tracker entry') || user_access('edit own time tracker entries') || user_access('administer time entries');

  $rows = array();
  $header = array(
    t('User'),
    t('Activity'),
    t('Duration'),
  );

  if ($use_deductions) {
    $header[] = t('Deductions');
    $header[] = t('Total');
  }

  $header[] = t('Time');
  $header[] = t('Note');

  if ($use_billable) {
    $header[] = t('Billable');
  }
  if ($use_billed) {
    $header[] = t('Billed');
  }
  if ($has_ops) {
    $header[] = t('Operations');
  }

  foreach ($time_entries as $time_entry) {
    if (!(
      user_access('view all time tracker entries') ||
      (user_access('view own time tracker entries') && $user->uid == $time_entry->uid)
    )) {
      continue;
    }

    $time = '';
    if (variable_get('time_entry_method', 'duration') == 'duration') {
      $time = format_date($time_entry->timestamp, 'custom', variable_get('timestamp_date_format', 'F d, Y'));
    }
    elseif (variable_get('time_entry_method', 'duration') == 'interval') {
      if ($time_entry->start && $time_entry->end) {
        $time = '' . format_date($time_entry->start, 'custom', variable_get('time_interval_date_format', 'h:i A - M d, Y')) . '<br/>' . format_date($time_entry->end, 'custom', variable_get('time_interval_date_format', 'h:i A - M d, Y'));
      }
      else {
        $time = format_date($time_entry->timestamp, 'custom', variable_get('timestamp_date_format', 'F d, Y')) . '<br/>' . t('Entered as Duration');
      }
    }

    $row = array();

    $row[] = array(
      'data' => user_load($time_entry->uid)->name,
      'class' => 'time_entry_username',
    );
    $row[] = array(
      'data' => filter_xss_admin(_time_tracker_activity_get_name($time_entry->activity)),
      'class' => 'time_entry_activity',
    );
    $row[] = array(
      'data' => _time_tracker_format_hours_to_hours_and_minutes($time_entry->duration),
      'class' => 'time_entry_duration',
    );

    if ($use_deductions) {
      $row[] = array(
        'data' => _time_tracker_format_hours_to_hours_and_minutes($time_entry->deductions),
        'class' => 'time_entry_deductions',
      );
      $row[] = array(
        'data' => _time_tracker_format_hours_to_hours_and_minutes($time_entry->duration - $time_entry->deductions),
        'class' => 'time_entry_total',
      );
    }

    $row[] = array(
      'data' => $time,
      'class' => 'time_entry_time',
    );
    $row[] = array(
      'data' => filter_xss($time_entry->note),
      'class' => 'time_entry_note',
    );

    if ($use_billable) {
      $row[] = array(
        'data' => $time_entry->billable ? t('Yes') : t('No'),
        'class' => 'time_entry_billable',
      );
    }
    if ($use_billed) {
      $row[] = array(
        'data' => $time_entry->billed ? t('Yes') : t('No'),
        'class' => 'time_entry_billed',
      );
    }
    if ($has_ops) {
      // If entry is locked and user does not have proper permissions.
      if (variable_get('allow_locked_time_entries', 0) && $time_entry->locked && !user_access('administer time entries')) {
        $row[] = array(
          'data' => t('locked'),
          'class' => 'time_entry_edit',
        );
      }
      else {
        if ($user->uid == $time_entry->uid || user_access('administer time entries')) {
          $destination = time_tracker_entry_uri($time_entry);
          $row[] = array(
            'data' => l(t('edit'), $destination['path']),
            'class' => 'time_entry_edit',
          );
        }
        else {
          $row[] = array(
            'data' => '',
            'class' => 'time_entry_edit',
          );
        }
      }
    }
    else {
      $row[] = array(
        'data' => '',
        'class' => 'time_entry_edit',
      );
    }

    $rows[] = $row;
  }

  $wrapper = array();
  $collapsed = variable_get('time_entry_table_default_collapsed', 0);
  $wrapper['entries'] = array(
    '#type' => 'fieldset',
    '#title' => t('Time Entries'),
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed,
    '#attributes' => array(
      'class' => array(
        'collapsible',
      ),
    ),
  );
  if ($collapsed) {
    $wrapper['entries']['#attributes']['class'][] = 'collapsed';
  }
  $wrapper['entries']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('class' => array('time_tracker_time_entries')),
  );
  return drupal_render($wrapper);
}

/**
 * Theme function for time entries on comments.
 *
 * @param array $variables
 *   An array containing data:
 *     A time entry object containing at least:
 *       teid: The time entry id;
 *       nid: The node id this time entry is attached to;
 *       uid: The user id to whom this time entry belongs;
 *       cid: If applicable, the comment id for this time entry;
 *       activity: The time entry Activity;
 *       timestamp: The unix timestamp of when this time entry was dated;
 *       start: If tracking time by interval, the start time of the entry;
 *       end: If tracking time by interval, the end time of the entry;
 *       duration: The time entry duration in HOURS;
 *       note: Any notes about the time entry (empty for comments);
 *       billable: Whether this is billable or not (0 => no, 1 => yes);
 *       locked: Whether this entry has been locked, requires extra permissions.
 *
 * @return string
 *   The themed form.
 *
 * @todo FIXME.
 */
function theme_time_tracker_comment($variables) {
  $time_entry = $variables['data'];
  if ($time_entry) {
    $rows = array();
    $time = '';

    $activity = filter_xss_admin(_time_tracker_activity_get_name($time_entry->activity));

    // If there is a time start and end, it's a time entry that was logged as an interval
    if ($time_entry->start && $time_entry->end) {
      //Setting up the table headers
      $header[] = array('data' => t('Duration'));
      if (variable_get('enable_deductions_field', 0)) {
        $header[] = array('data' => t('Deductions'));
        $header[] = array('data' => t('Total'));
      }
      $header[] = array('data' => t('Activity'));
      $header[] = array('data' => t('Start'));
      $header[] = array('data' => t('End'));
      if (variable_get('enable_billable_field', 0)) {
        $header[] = array('data' => t('Billable'));
      }
      if (variable_get('enable_billed_field', 0)) {
        $header[] = array('data' => t('Billed'));
      }

      $row = array(//row
        'data' => array( //row data
          array( //Cell3
            'data' => _time_tracker_format_hours_to_hours_and_minutes($time_entry->duration),
            'class' => 'time_entry_duration',
          ),
        ),
        'class' => '', //row class
      );//endrow
      if (variable_get('enable_deductions_field', 0)) {
        $row['data'][] = array(
          'data' => _time_tracker_format_hours_to_hours_and_minutes($time_entry->deductions),
          'class' => 'time_entry_deductions',
        );
        $row['data'][] = array(
          'data' => _time_tracker_format_hours_to_hours_and_minutes($time_entry->duration - $time_entry->deductions),
          'class' => 'time_entry_total',
        );
      }
      $row['data'][] = array(
        'data' => $activity,
        'class' => 'time_entry_activity',
      );
      $row['data'][] = array(
        'data' => format_date($time_entry->start, 'custom', variable_get('time_interval_date_format', 'h:i A - M d, Y')),
        'class' => 'time_entry_start',
      );
      $row['data'][] = array(
        'data' => format_date($time_entry->end, 'custom', variable_get('time_interval_date_format', 'h:i A - M d, Y')),
        'class' => 'time_entry_end',
      );
      if (variable_get('enable_billable_field', 0)) {
        $row['data'][] = array( //Cell6
          'data' => $time_entry->billable ? t('Yes') : t('No'),
          'class' => 'time_entry_billable',
        );
      }
      if (variable_get('enable_billed_field', 0)) {
        $row['data'][] = array( //Cell6
          'data' => $time_entry->billed ? t('Yes') : t('No'),
          'class' => 'time_entry_billed',
        );
      }
      $rows[] = $row;
      $table = array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('class' => array('time_tracker_entry_comment')),
      );
      return theme('table', $table);
    }
    else {
      $time_string = array();
      $time_string['total'] = _time_tracker_format_hours_to_hours_and_minutes($time_entry->duration - $time_entry->deductions);
      if (variable_get('enable_deductions_field', 0)) {
        $time_string['total_details'] = '(' . _time_tracker_format_hours_to_hours_and_minutes($time_entry->duration) . ' - ' . _time_tracker_format_hours_to_hours_and_minutes($time_entry->deductions) . ')';
      }
      $time_string['activity'] = $activity ? '(' . $activity . ') ' : $activity;
      $time_string['on'] = t('on');
      $time_string['time'] = format_date($time_entry->timestamp, 'custom', variable_get('timestamp_date_format', 'F d, Y'));
      if (variable_get('enable_billable_field', 0)) {
        $time_string['billable'] = $time_entry->billable ? t('Billable: Yes.') : t('Billable: No.');
      }
      if (variable_get('enable_billed_field', 0)) {
        $time_string['billed'] = $time_entry->billed ? t('Billed: Yes.') : t('Billed: No.');
      }

      $rows[] = array(t('Time') . ': ' . implode(' ', $time_string));

      $table = array(
        'rows' => $rows,
        'attributes' => array('class' => array('time_tracker_entry_comment')),
      );

      return theme('table', $table);
    }
  }

  return '';
}

/**
 * Theme function for the time tracker entry summary.
 *
 * @param array $variables
 *   An array containing total_time:
 *    Amount of time in hours (float).
 *
 * @return string
 *    A themed table of summary data.
 */
function theme_time_tracker_summary($variables) {
  $total_time = $variables['total_time'];
  $rows = array();

  if ($total_time > 0) {
    $rows[] = array(
      t('Time logged:') . ' ',
      '<span class="time_tracker_time">' . _time_tracker_format_hours_to_hours_and_minutes($total_time, FALSE, TRUE) . '</span>',
    );
  }
  $table = array(
    'rows' => $rows,
    'attributes' => array('class' => array('time_tracker_summary')),
  );

  return theme('table', $table);
}


/**
 * Preprocess function that determines whether to show the comment or not.
 *
 * The $hide variable is already in the template file (comment.tpl.php)
 * it's just not being used right now by any other modules... so let's use it!
 */
function time_tracker_preprocess_comment(&$vars) {
  $comment = $vars['comment'];
  if ($comment->status == 2 && variable_get('hide_comments', 0) && !user_access('view all time tracker entries')) {
    $vars['hide'] = TRUE;
  }
}


