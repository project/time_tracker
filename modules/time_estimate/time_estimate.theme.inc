<?php

/**
 * @file
 * Time estimate theme callbacks.
 */

/**
 * Theme function for the time estimate summary.
 *
 * This theme function is meant to replace the Time Tracker Summary output by
 * theme_time_tracker_summary in module time_tracker.
 *
 * @param array $variables
 *   An array containing at least the following keys:
 *   - "time": the time logged so far.
 *   - "estimate": the time estimate in hours.
 *
 * @return string
 *   The time estimate summary.
 */
function theme_time_estimate_summary($variables) {
  $time = (float) $variables['time'];
  $estimate = (float) $variables['estimate'];
  $rows = array();

  if ($estimate > $time) {
    $class = 'time_under';
    $remaining = t('!time remaining', array(
      '!time' => _time_tracker_format_hours_to_hours_and_minutes($estimate - $time, FALSE, TRUE),
    ));
  }
  else {
    $class = 'time_over';
    $remaining = t('over by: !time', array(
      '!time' => _time_tracker_format_hours_to_hours_and_minutes(abs($estimate - $time), FALSE, TRUE),
    ));
  }

  $headers = array(
    array('data' => t('Estimated Time:')),
    array('data' => t('Time Logged:')),
  );

  $rows[] = array(
    array(
      'data' => '<span class="time_estimate_time">' . _time_tracker_format_hours_to_hours_and_minutes($estimate) . '</span>',
      'class' => array('time_estimate'),
    ),
    array(
      'data' => '<span class="time_tracker_time">' . _time_tracker_format_hours_to_hours_and_minutes($time) . '</span> <span class="time_tracker_remaining ' . $class . '">(' . $remaining . ')</span>',
      'class' => array('time_tracker'),
    ),
  );
  $table = array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array('class' => array('time_estimate_summary')),
  );

  return theme('table', $table);
}

