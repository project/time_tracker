<?php

/**
 * @file
 * Time sheet theme callbacks.
 */

/**
 * Theme function for the time_sheet user page.
 *
 * This callback displays a users time_sheet defaulting to the current week.
 *
 * @param array $variables
 *   An array of containing at least:
 *   - "user": the user for whom we are displaying the time_sheet.
 *
 * @return string
 *   A themed table of time entry data.
 */
function theme_time_sheet_user_page($variables) {
  drupal_add_css(drupal_get_path('module', 'time_sheet') . '/css/time_sheet.css');

  if (is_null($variables['user'])) {
    global $user;
    $account = $user;
  }
  else {
    $account = $variables['user'];
  }

  // Get a start timestamp from the url parameters or set a default.
  if (!isset($_GET['start'])) {
    $start = time();
  }
  else {
    $start = $_GET['start'];
  }

  // Create a lookup table of time stamps for the week.
  $date_lookup = array(
    'sunday' => strtotime(date('Y\WW0', $start)),
    'monday' => strtotime(date('Y\WW1', $start)),
    'tuesday' => strtotime(date('Y\WW2', $start)),
    'wednesday' => strtotime(date('Y\WW3', $start)),
    'thursday' => strtotime(date('Y\WW4', $start)),
    'friday' => strtotime(date('Y\WW5', $start)),
    'saturday' => strtotime(date('Y\WW6', $start)),
  );

  // Get the configured days of the week.
  $days_of_week = variable_get('time_sheet_days_of_week', array(
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
  ));
  // Get the configured first day of the week.
  $first_day = variable_get('time_sheet_week_start', 'monday');

  // A variable to store days at the beginning of the week to be moved to the
  // end of the week.
  $days_at_end = array();
  // Remove items at the beginning of the week until we get to the configured
  // $first_day.
  while (current($days_of_week) != $first_day && current($days_of_week)) {
    $days_at_end[] = array_shift($days_of_week);
  }

  // Put the days from the beginning at the end.
  foreach ($days_at_end as $day) {
    $days_of_week[$day] = $day;
  }
  // If we started the week after sunday, the days that we move to the end
  // should be for next week, not week before.
  foreach ($days_of_week as $key => $value) {
    if ($value != '0') {
      if (!in_array($value, $days_at_end)) {
        $days[$value] = $date_lookup[$value];
      }
      else {
        $days[$value] = strtotime('+ 1 week', $date_lookup[$value]);
      }
    }
  }

  // Determine the neighbouring weeks timestamps, because of the way we
  // structured the week, the next week will start on the 8th day after
  // $first day. And the previous week will end 1 day before the $first day.
  $next_week = strtotime('+ 8 days', $days[$first_day]);
  $prev_week = strtotime('- 1 days', $days[$first_day]);

  // Output the week switcher.
  $output = '<span class="time-sheet-header">';
  $output .= l(t('&laquo;'), $_GET['q'], array(
    'query' => array('start' => $prev_week),
    'html' => 'TRUE',
  ));
  $output .= ' ' . date("l, M jS", reset($days)) . ' - ' . date("l, M jS", end($days)) . ' ';
  $output .= l(t('&raquo;'), $_GET['q'], array(
    'query' => array('start' => $next_week),
    'html' => 'TRUE',
  ));
  $output .= '</span>';

  // Loop through days of the week pulling time entries and generating a table.
  foreach ($days as $day => $stamp) {
    // The header displaying the date.
    $header = array(array('data' => date("l, M jS Y", $stamp), 'colspan' => 3));
    // Rows Variable for later theme_table-ing.
    $rows = array();
    // Get the all the time tracker entries for the date we're on.
    if (isset($variables['user'])) {
      $time_entrys = db_select('time_tracker_entry', 'tte')
        ->fields('tte')
        ->condition('uid', $account->uid, '=')
        ->condition('timestamp', array(
          $stamp,
          strtotime("+1 day", $stamp) - 1,
          'BEWTEEN',
        ))
        ->orderBy('teid')
        ->execute();
    }
    else {
      if (user_access('view all time tracker entries')) {
        $time_entrys = db_select('time_tracker_entry', 'tte')
          ->fields('tte')
          ->condition('timestamp', array(
            $stamp,
            strtotime("+1 day", $stamp) - 1,
            'BEWTEEN',
          ))
          ->orderBy('teid')
          ->execute();
      }
      else {
        $time_entrys = db_select('time_tracker_entry', 'tte')
          ->fields('tte')
          ->condition('uid', $account->uid, '=')
          ->condition('timestamp', array(
            $stamp,
            strtotime("+1 day", $stamp) - 1,
            'BEWTEEN',
          ))
          ->orderBy('teid')
          ->execute();
      }
    }

    $total_duration = 0;
    // Loop through each.
    while ($time_entry = $time_entrys->fetchObject()) {
      // Container for row data to display a time entry.
      $row = array();
      // Load the time entry node object.
      $time_user = user_load($time_entry->uid);
      $task = entity_load($time_entry->entity_type, array($time_entry->entity_id));
      $task = $task[$time_entry->entity_id];
      $turi = entity_uri($time_entry->entity_type, $task);
      $row[] = l($task->title, $turi['path'], array('attributes' => array('class' => 'time-sheet-task')));
      // The little colorful project icon next to time entries in the time_sheet
      // And the time_entry item name. Both linked.
      $row[] = l($time_user->name, 'user/' . $time_user->uid, array('attributes' => array('class' => 'time-sheet-user')));
      // The amount of time logged for this item.
      $row[] = array(
        'data' => _time_tracker_format_hours_to_hours_and_minutes($time_entry->duration - $time_entry->deductions),
        'align' => 'right',
      );
      // On going add-up of the total duration of time logged this week.
      $total_duration = $total_duration + ($time_entry->duration - $time_entry->deductions);
      // Add the row to the rows.
      $rows[] = array('data' => $row);
    }

    // As long as this day has a time entry, the row array will be populated
    // with more than 1 item.
    if (isset($row) && count($row) > 1) {
      $row = array();
      $row[] = array(
        'data' => '<strong>' . t("Total hours for") . " " . date("l", $stamp) . '</strong>',
        'colspan' => 2,
      );

      $row[] = array(
        'data' => '<strong>' . _time_tracker_format_hours_to_hours_and_minutes($total_duration) . '</strong>',
        'align' => 'right',
      );
      $rows[] = array('data' => $row);
    }
    else {
      $row = array();
      $row[] = array('data' => t("No time entered."), 'colspan' => 3);
      $rows[] = array('data' => $row);
    }

    $row = array();

    $table = array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('id' => array('time-sheet-' . $day)),
    );

    // Add this day to the table.
    $output .= theme('table', $table);
  }

  return $output;
}
