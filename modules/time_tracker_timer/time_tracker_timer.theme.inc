<?php

/**
 * @file
 * The theming functions for time_tracker_time.
 */

/**
 * Theme function for the timer control bar on entities.
 *
 * @param array $variables
 *   An array of variables that must contain the following keys:
 *   - 'bundle': the bundle;
 *   - 'type': the entity type;
 *   - 'entity': the entity;
 *   - 'timers': the timers.
 *
 * @return string
 *   The themed timer messages.
 */
function theme_time_tracker_timer($variables) {
  $bundle = $variables['bundle'];
  $type = $variables['type'];
  $entity = $variables['entity'];
  $timers = $variables['timers'];

  global $user;

  // Rows for timer messages about this user's timers.
  $rows = array();
  $has_timer = FALSE;
  $no_resume = variable_get('time_entry_method', 'duration') == 'interval' && !variable_get('enable_deductions_field', 0) ? ' no_resume' : '';
  drupal_add_js(drupal_get_path('module', 'time_tracker_timer') . '/js/time_tracker_timer.js');

  // Loop through the results.
  // Should only be ONE if user doesn't have 'view all' privileges.
  foreach ($timers as $timer) {
    // If the timer entry uid matches the current user then we process normally.
    if ($timer->uid == $user->uid) {
      $has_timer = TRUE;
      // If the timer doesn't have a stop value, it's still running.
      if ($timer->stop == 0) {
        // Change the timer state to be in "active" state.
        $timer_state = 'active';
        $total_time = time() - $timer->start + $timer->total;
        $msg = t('Your timer is currently active.');
      }
      else {
        // If the timer has a stop value, then it has been stopped but not saved
        // since it still exists (will be deleted when stopped and saved).
        // Set the timer state to 'stopped' state.
        $timer_state = 'stopped';
        $turi = entity_uri($type, $entity);
        $hours_field_link = $turi['path'] . (time_tracker_is_tracking_time($type, $bundle) == 'entity' ? '#time-tracker-time-entry-form' : '#comment-form');
        $total_time = $timer->total ? $timer->total : $timer->stop - $timer->start;
        $msg = t('Your time has been entered below.  <a href="!url">Please complete the form</a>', array(
          '!url' => check_url(base_path() . $hours_field_link),
        ));
      }
      // Format the rows for eventual theme_table.
      $rows[] = array(
        'data' => array(
          array(
            'data' => drupal_get_form('time_tracker_timer_startstop_form_' . $timer->ttid, $entity, $bundle, $type, $timer_state, $timer->start),
            'class' => $timer_state . '_timer_control timer_mine timer_control' . ($timer_state == 'stopped' ? $no_resume : ''),
          ),
          array(
            'data' => _time_tracker_format_secs_to_hhmmss($total_time),
            'class' => $timer_state . '_timer_time timer_mine timer_time',
          ),
          array(
            'data' => $msg,
            'class' => 'active_timer_msg timer_mine timer_msg',
          ),
        ),
      );
    }
    else {
      // If the timer entry uid doesn't match the user, then we just want to add
      // it to the page to view, but not affect the how the timer button will
      // work (i.e. start, stop, reset).
      // Load the user name.
      $username = user_load($timer->uid)->name;
      if ($timer->stop == 0) {
        $timer_state = 'active';
        $total_time = time() - $timer->start + $timer->total;
        $msg = t('@name has an unsaved timer entry for this ticket', array('@name' => $username));
      }
      else {
        // If the timer has a stop value, then it has been stopped but not saved
        // since it still exists (should be deleted when stopped and saved).
        // Set the timer state to 'stopped' state.
        $timer_state = 'stopped';
        $total_time = $timer->total ? $timer->total : $timer->stop - $timer->start;
        $msg = t('@name has an unsaved timer entry for this ticket', array('@name' => $username));
      }
      $rows[] = array(
        'data' => array(
          array(
            'data' => drupal_get_form('time_tracker_timer_startstop_form_' . $timer->ttid, $entity, $bundle, $type, $timer_state, $timer->start, $timer->uid),
            'class' => $timer_state . '_timer_control timer_control' . ($timer_state == 'stopped' ? $no_resume : ''),
          ),
          array(
            'data' => _time_tracker_format_secs_to_hhmmss($total_time),
            'class' => $timer_state . '_timer_time timer_time',
          ),
          array(
            'data' => $msg,
            'class' => $timer_state . '_timer_msg timer_msg',
          ),
        ),
      );
    }
  }
  // If no rows were added there are no timer entries and the 'start timer' row
  // must be displayed.
  if (!$has_timer) {
    $timer_state = 'new';
    array_unshift($rows, array(
      'data' => array(
        array(
          'data' => drupal_get_form('time_tracker_timer_startstop_form_start', $entity, $bundle, $type, $timer_state),
          'class' => $timer_state . '_timer_control timer_control',
        ),
        array(
          'data' => _time_tracker_format_secs_to_hhmmss(0),
          'class' => $timer_state . '_timer_time timer_time',
        ),
        array(
          'data' => t('Click <em>start timer</em> to begin tracking your time'),
          'class' => $timer_state . '_timer_msg timer_msg',
        ),
      ),
    ));
  }

  $table = array(
    'header' => NULL,
    'rows' => $rows,
    'attributes' => array('class' => 'time_tracker_timer'),
  );

  return theme('table', $table);
}

/**
 * Theme function for active time_tracker_timer timers page.
 *
 * @param array $variables
 *   An array containing:
 *   - 'active_timers':  The list of current active timers to be listed.
 *
 * @return string
 *   A themed table of active timers.
 */
function theme_time_tracker_timer_active_timers_page($variables) {
  $active_timers = $variables['active_timers'];

  drupal_add_js(drupal_get_path('module', 'time_tracker_timer') . '/js/time_tracker_timer.js');

  // Rows for current user timer messages.
  $rows = array();
  $image = theme('image', array(
    'path' => drupal_get_path('module', 'time_tracker_timer') . '/images/time_tracker_stopwatch_icon_white.png',
    'alt' => t('Time Tracker Timer Icon'),
    'title' => 'timer icon',
  ));

  // Loop through the results.
  foreach ($active_timers as $timer) {
    $entity = entity_load($timer->entity_type, array($timer->entity_id));
    $entity = reset($entity);
    $uri = entity_uri($timer->entity_type, $entity);
    $timer_links = l($entity->title, $uri['path']);

    // If the timer doesn't have a stop value, it's still running.
    if ($timer->stop == 0) {
      // Change the timer state to be in "active" state.
      $timer_state = 'active';
      $total_time = time() - $timer->start + $timer->total;
      $timer_msg = t('!username has an active timer on !timer_links', array(
        '!username' => theme('username', array('account' => user_load($timer->uid))),
        '!timer_links' => $timer_links,
      ));
    }
    else {
      // If the timer has a stop value, then it has been stopped but not saved
      // since it still exists (will be deleted when stopped and saved).
      // Set the timer state to 'stopped' state.
      $timer_state = 'stopped';
      $timer_links = l($entity->title, $uri['path'] . '#edit-time-wrapper');
      $total_time = $timer->total ? $timer->total : $timer->stop - $timer->start;
      $timer_msg = t('!username has an active timer on !timer_links', array(
        '!username' => theme('username', array('account' => user_load($timer->uid))),
        '!timer_links' => $timer_links,
      ));
    }
    // Format the rows for eventual theme_table.
    $rows[] = array(
      'data' => array(
        array(
          'data' => $image,
          'class' => 'timer_page_icon',
        ),
        array(
          'data' => drupal_get_form('time_tracker_timer_startstop_form_' . $timer->ttid, $entity, $timer->entity_bundle, $timer->entity_type, $timer_state, $timer->start),
          'class' => $timer_state . '_timer_control timer_control',
        ),
        array(
          'data' => _time_tracker_format_secs_to_hhmmss($total_time),
          'class' => $timer_state . '_timer_time timer_page_time',
        ),
        array(
          'data' => $timer_msg,
          'class' => 'active_timer_page_msg timer_page_msg',
        ),
      ),
    );
  }

  $table = array(
    'header' => NULL,
    'rows' => $rows,
    'attributes' => array('class' => 'active_timers_page'),
    'empty' => t('There are no active timers at this time'),
  );

  return theme('table', $table);
}

/**
 * Theme function for active time_tracker_timer timers block.
 *
 * @param array $variables
 *   An array containing:
 *   - 'active_timers':  The list of current active timers to be listed.
 *
 * @return string
 *   The themed table.
 */
function theme_time_tracker_timer_active_timers_block($variables) {
  $active_timers = $variables['active_timers'];

  drupal_add_js(drupal_get_path('module', 'time_tracker_timer') . '/js/time_tracker_timer.js');

  $rows = array();
  $image = theme('image', array(
    'path' => drupal_get_path('module', 'time_tracker_timer') . '/images/time_tracker_stopwatch_icon_white.png',
    'alt' => t('Time Tracker Timer Icon'),
    'title' => 'timer icon',
  ));

  // Loop through the results.
  foreach ($active_timers as $timer) {
    $entity = entity_load($timer->entity_type, array($timer->entity_id));
    $entity = reset($entity);
    $uri = entity_uri($timer->entity_type, $entity);
    $timer_links = l($entity->title, $uri['path']);

    // If the timer doesn't have a stop value, it's still running.
    if ($timer->stop == 0) {
      // Change the timer state to be in "active" state.
      $timer_state = 'active';
      $total_time = time() - $timer->start + $timer->total;
      $timer_msg = t('You have an active timer on !links', array('!links' => $timer_links));
    }
    else {
      // If the timer has a stop value, then it has been stopped but not saved
      // since it still exists (will be deleted when stopped and saved).
      // Set the timer state to 'stopped' state.
      $timer_state = 'stopped';
      $timer_links = l($entity->title, $uri['path'] . '#edit-time-wrapper');
      $total_time = $timer->total ? $timer->total : $timer->stop - $timer->start;
      $timer_msg = t('You have an unsaved timer on !links.', array('!links' => $timer_links));
    }
    // Format the rows for eventual theme_table.
    $rows[] = array(
      'data' => array(
        array(
          'data' => $image,
          'class' => 'timer_block_icon',
        ),
        array(
          'data' => drupal_get_form('time_tracker_timer_startstop_form_' . $timer->ttid, $entity, $timer->entity_bundle, $timer->entity_type, $timer_state, $timer->start),
          'class' => $timer_state . '_timer_control timer_control',
        ),
        array(
          'data' => _time_tracker_format_secs_to_hhmmss($total_time),
          'class' => $timer_state . '_timer_time timer_block_time',
        ),
      ),
    );
    $rows[] = array(
      'data' => array(
        array(
          'data' => $timer_msg,
          'class' => 'active_timer_block_msg timer_block_msg',
          'colspan' => 3,
        ),
      ),
    );
  }

  $output = t('You have no active timers at this time.');
  if (!empty($rows)) {
    $table = array(
      'header' => NULL,
      'rows' => $rows,
      'attributes' => array('class' => 'active_timers_block'),
    );
    $output = theme('table', $table);
  }

  if (user_access('view all timers')) {
    $output .= '<div class="see_all_timers">' . l(t('See all active timers'), 'time_tracker_timer/active') . '</div>';
  }

  return $output;
}
